# FairSSD Understanding Bias in Synthetic Speech Detectors

This is the official repository of *FairSSD Understanding Bias in Synthetic Speech Detectors*, which is accepted at 2024 IEEE/CVF Conference on Computer Vision and Pattern Recognition (CVPR), Media Forensics Workshop (MFW).

Authors: Amit Kumar Singh Yadav, Kratika Bhagtani, Davide Salvi, Paolo Bestagini, Edward J. Delp

Please direct all correspondences to Prof. Edward J. Delp (ace@purdue.edu).


## Installation

- Coming Soon

## Dataset Used in the Study

- Coming Soon

## Training Synthetic Speech Detectors Used in the Study

- Coming Soon

## Conducting Bias Study Reported in the Study

- Coming Soon

## Citing

If you find our repository and work useful, please cite our paper.
```
@article{yadav_cvprw2024,
  author={Amit Kumar Singh Yadav and Kratika Bhagtani and Davide Salvi and Paolo Bestagini and Edward J. Delp},
  title={{FairSSD: Understanding Bias in Synthetic Speech Detectors}},
  year={2024},
  journal={Proceeding of the IEEE/CVF Conference on  Computer Vision and Pattern Recognition Workshop},
  month={June},
  note={{Seattle, USA}}
}
```
